## EMPOROS TEST

The project is based on a Clean Architecture template by Jason Taylor for NetCore 3.1

```bash
ASP.Net core 3.1
Entityframework Core SqlServer 3.1.10
Automapper
Fluent Validator
MediatR
IdentityServer4 
AspNetCore Versioning 
```

## Get started

1. Clone this project

2. Execute migration...

```bash
cd \src\WebApi
dotnet ef database update -c ApplicationDbContext
```

3 To get the token OAuth 2.0 : 

```bash
URL: https://localhost:5001/connect/token
Client Id: oscar_client
Client Secret: oscar_secret
Scope: apiEmporos
```
*Plase note that its using a dev certificate so SSL validation should be disabled.


## Other

- The list of endpotns: https://localhost:5001/swagger/index.html

- Versioning is configured using a Header : "api-version", it can be tested with the endpoint  GET /UnitsOfMeasure

- To create a new migration:
```bash
cd \src\WebApi
dotnet ef migrations add "MigrationName" -o Persistence/Migrations/Application --project ../Infrastructure/ -c ApplicationDbContext
```