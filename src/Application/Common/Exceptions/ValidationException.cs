﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentValidation.Results;

namespace OGM.EmporosTest.Application.Common.Exceptions
{
    public class ValidationErrorResponseDto
    {
        public IDictionary<string, string[]> ValidationErrors { get; set; } = new Dictionary<string, string[]>();
    }
    public class ValidationException : Exception
    {

        public ValidationErrorResponseDto ValidationResponse { get; }
        public ValidationException()
            : base("One or more validation failures have occurred.")
        {
            ValidationResponse = new ValidationErrorResponseDto();
        }
        public ValidationException(IEnumerable<ValidationFailure> failures)
            : this()
        {
            var failureGroups = failures
                .GroupBy(e => e.PropertyName, e => e.ErrorMessage);

            foreach (var failureGroup in failureGroups)
            {
                var propertyName = failureGroup.Key;
                var propertyFailures = failureGroup.ToArray();

                ValidationResponse.ValidationErrors.Add(propertyName, propertyFailures);
            }
        }


    }
}