﻿using System;

namespace OGM.EmporosTest.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}
