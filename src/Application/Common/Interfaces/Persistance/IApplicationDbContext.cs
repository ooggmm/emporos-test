﻿using OGM.EmporosTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Common.Interfaces.Persistance
{
    public interface IApplicationDbContext
    {
        public DbSet<Hospital> Hospitals { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<ItemVendor> ItemVendors { get; set; }
        public DbSet<Pharmacy> Pharmacies { get; set; }
        public DbSet<PharmacyInventory> PharmaciesInventory { get; set; }
        public DbSet<UnitOfMesure> UnitsOfMesure { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task BeginTransactionAsync();
        Task CommitTransactionAsync();
        void RollbackTransaction();
    }
       
}
