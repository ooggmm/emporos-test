﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OGM.EmporosTest.Application.Common.Models
{
    public static class Response
    {

        public static Response<T> Fail<T>(string message, T data = default) => new Response<T>(data, message, false);
        public static Response<T> Ok<T>(string message, T data = default) => new Response<T>(data, message ?? "Success", true);
        public static Response<T> Ok<T>(T data = default) => new Response<T>(data, "Success", true);
    }
    public class Response<T>
    {
        public Response(T data, string message, bool success)
        {
            Message = message;
            Data = data;
            Success = success;
        }
        public bool Success { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }

    }
}
