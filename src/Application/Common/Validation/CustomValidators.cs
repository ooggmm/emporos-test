﻿using FluentValidation;
using FluentValidation.Validators;

namespace OGM.EmporosTest.Application.Common.Validation
{
    public static class CustomValidators
    {
        public static IRuleBuilderOptions<T, string> MatchNumericRule<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new RegularExpressionValidator("^[0-9]*$"));
        }
    }
}
