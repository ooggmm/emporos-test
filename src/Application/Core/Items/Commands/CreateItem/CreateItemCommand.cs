﻿using AutoMapper;
using MediatR;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Application.Common.Models;
using OGM.EmporosTest.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.Items.Commands.CreateItem
{
    public class CreateItemCommand : IRequest<Response<int>>, IMapTo<Item>
    {
        public int ItemVendorId { get; set; }
        public string Upc { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public int MinimumOrderQuantity { get; set; }
        public int PurchaseUnitOfMesureId { get; set; }
    }

    public class CreateItemCommandHandler : IRequestHandler<CreateItemCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

        public CreateItemCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<Response<int>> Handle(CreateItemCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<Item>(request);

            _context.Items.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Response.Ok(entity.Id);
        }
    }



}
