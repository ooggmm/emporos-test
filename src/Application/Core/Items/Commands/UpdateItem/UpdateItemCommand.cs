﻿using MediatR;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Application.Common.Models;
using OGM.EmporosTest.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.Items.Commands.UpdateItem
{
    public class UpdateItemCommand : IRequest<Response<Unit>>
    {
        public int Id { get; set; }
        public int ItemVendorId { get; set; }
        public string Upc { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public int MinimumOrderQuantity { get; set; }
        public int PurchaseUnitOfMesureId { get; set; }
    }
    public class UpdateItemCommandHandler : IRequestHandler<UpdateItemCommand, Response<Unit>>
    {
        private readonly IApplicationDbContext _context;

        public UpdateItemCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Response<Unit>> Handle(UpdateItemCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Items.FindAsync(request.Id);

            entity.ItemVendorId = request.ItemVendorId;
            entity.Upc = request.Upc;
            entity.Description = request.Description;
            entity.Cost = request.Cost;
            entity.MinimumOrderQuantity = request.MinimumOrderQuantity;
            entity.PurchaseUnitOfMesureId = request.PurchaseUnitOfMesureId;

            _context.Items.Update(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Response.Ok<Unit>();
        }
    }


}
