﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Validation;
using System.Linq;

namespace OGM.EmporosTest.Application.Core.Items.Commands.UpdateItem
{

    public class UpdateItemCommandValidator : AbstractValidator<UpdateItemCommand>
    {
        public UpdateItemCommandValidator(IApplicationDbContext _context)
        {

            RuleFor(x => x.Id)
                .Must(x => _context.Items.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid Item Id");

            RuleFor(x => x.ItemVendorId)
                .Must(x => _context.ItemVendors.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid ItemVendorId");

            RuleFor(x => x.PurchaseUnitOfMesureId)
              .Must(x => _context.UnitsOfMesure.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid UnitOfMesureId");

            RuleFor(x => x.Description)
              .NotEmpty()
              .MaximumLength(100);

            RuleFor(x => x.MinimumOrderQuantity)
              .GreaterThan(0);

            RuleFor(x => x.Cost)
              .GreaterThan(0);

            RuleFor(x => x.Upc)
              .MatchNumericRule().WithMessage(x => $"Numeric values Only")
              .Length(12);

        }
    }
}
