﻿using AutoMapper;
using MediatR;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Application.Common.Models;
using OGM.EmporosTest.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.CreatePharmacyInventoryRecord
{
    public class CreatePharmacyInventoryRecordCommand : IRequest<Response<int>>, IMapTo<PharmacyInventory>
    {
        public int ItemId { get; set; }
        public int PharmacyId { get; set; }
        public int QuantityOnHand { get; set; }
        public decimal UnitPrice { get; set; }
        public int ReorderQuantity { get; set; }
        public int SellingUnitOfMesureId { get; set; }
    }

    public class CreatePharmacyInventoryRecordCommandHandler : IRequestHandler<CreatePharmacyInventoryRecordCommand, Response<int>>
    {
        private readonly IMapper _mapper;
        private readonly IApplicationDbContext _context;

        public CreatePharmacyInventoryRecordCommandHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<Response<int>> Handle(CreatePharmacyInventoryRecordCommand request, CancellationToken cancellationToken)
        {
            var entity = _mapper.Map<PharmacyInventory>(request);

            _context.PharmaciesInventory.Add(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return  Response.Ok(entity.Id);
        }
    }
}
