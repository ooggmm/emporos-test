﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using System;
using System.Linq;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.CreatePharmacyInventoryRecord
{
    public class CreatePharmacyInventoryRecordCommandValidator : AbstractValidator<CreatePharmacyInventoryRecordCommand>
    {
        public CreatePharmacyInventoryRecordCommandValidator(IApplicationDbContext _context)
        {
            
            RuleFor(x => x.PharmacyId)
                .Must(x => _context.Pharmacies.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid PharmacyId");
            
            RuleFor(x => x.ItemId)
              .Must(x => _context.Items.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid ItemId");

            RuleFor(x => x.SellingUnitOfMesureId)
              .Must(x => _context.UnitsOfMesure.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid UnitOfMesureId");


            RuleFor(x => new {x.PharmacyId , x.ItemId })
              .Must(x => !_context.PharmaciesInventory.AsNoTracking().Any(c => c.ItemId == x.ItemId && c.PharmacyId == x.PharmacyId)).WithMessage(x => $"Record Already registered").WithName("PharmacyId,ItemId");

            RuleFor(x => x.QuantityOnHand)
              .GreaterThan(0);

            RuleFor(x => x.ReorderQuantity)
              .GreaterThan(0);

            RuleFor(x => x.UnitPrice)
              .GreaterThan(0);

        }
    }
}
