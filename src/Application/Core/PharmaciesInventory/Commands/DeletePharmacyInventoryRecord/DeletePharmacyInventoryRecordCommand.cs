﻿using AutoMapper;
using MediatR;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Application.Common.Models;
using OGM.EmporosTest.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.DeletePharmacyInventoryRecord
{
    public class DeletePharmacyInventoryRecordCommand : IRequest<Response<Unit>>
    {
        public int Id { get; set; }
    }

    public class DeletePharmacyInventoryRecordCommandHandler : IRequestHandler<DeletePharmacyInventoryRecordCommand, Response<Unit>>
    {
        private readonly IApplicationDbContext _context;

        public DeletePharmacyInventoryRecordCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Response<Unit>> Handle(DeletePharmacyInventoryRecordCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PharmaciesInventory.FindAsync(request.Id);
            _context.PharmaciesInventory.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return  Response.Ok<Unit>();
        }
    }
}
