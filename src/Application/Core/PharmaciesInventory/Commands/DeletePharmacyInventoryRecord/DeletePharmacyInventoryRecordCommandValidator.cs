﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using System;
using System.Linq;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.DeletePharmacyInventoryRecord
{
    public class DeletePharmacyInventoryRecordCommandValidator : AbstractValidator<DeletePharmacyInventoryRecordCommand>
    {
        public DeletePharmacyInventoryRecordCommandValidator(IApplicationDbContext _context)
        {
            
            RuleFor(x => x.Id)
                .Must(x => _context.PharmaciesInventory.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid Id");
        }
    }
}
