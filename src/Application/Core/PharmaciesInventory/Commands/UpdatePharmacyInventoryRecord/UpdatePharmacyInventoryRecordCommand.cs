﻿using AutoMapper;
using MediatR;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Application.Common.Models;
using OGM.EmporosTest.Domain.Entities;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.UpdatePharmacyInventoryRecord
{
    public class UpdatePharmacyInventoryRecordCommand : IRequest<Response<Unit>>
    {
        public int Id { get; set; }
        public int QuantityOnHand { get; set; }
        public decimal UnitPrice { get; set; }
        public int ReorderQuantity { get; set; }
        public int SellingUnitOfMesureId { get; set; }
    }

    public class UpdatePharmacyInventoryRecordCommandHandler : IRequestHandler<UpdatePharmacyInventoryRecordCommand, Response<Unit>>
    {
        private readonly IApplicationDbContext _context;

        public UpdatePharmacyInventoryRecordCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }
        public async Task<Response<Unit>> Handle(UpdatePharmacyInventoryRecordCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.PharmaciesInventory.FindAsync(request.Id);

            entity.QuantityOnHand = request.QuantityOnHand;
            entity.UnitPrice = request.UnitPrice;
            entity.ReorderQuantity = request.ReorderQuantity;
            entity.SellingUnitOfMesureId = request.SellingUnitOfMesureId;

            _context.PharmaciesInventory.Update(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return  Response.Ok<Unit>();
        }
    }
}
