﻿using FluentValidation;
using Microsoft.EntityFrameworkCore;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using System;
using System.Linq;

namespace OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.UpdatePharmacyInventoryRecord
{
    public class UpdatePharmacyInventoryRecordCommandValidator : AbstractValidator<UpdatePharmacyInventoryRecordCommand>
    {
        public UpdatePharmacyInventoryRecordCommandValidator(IApplicationDbContext _context)
        {
            
            RuleFor(x => x.Id)
                .Must(x => _context.PharmaciesInventory.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid Id");
            
            RuleFor(x => x.SellingUnitOfMesureId)
              .Must(x => _context.UnitsOfMesure.AsNoTracking().Any(c => c.Id == x)).WithMessage(x => $"Invalid UnitOfMesureId");

            RuleFor(x => x.QuantityOnHand)
              .GreaterThan(0);

            RuleFor(x => x.ReorderQuantity)
              .GreaterThan(0);

            RuleFor(x => x.UnitPrice)
              .GreaterThan(0);

        }
    }
}
