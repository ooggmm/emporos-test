﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OGM.EmporosTest.Application.Common.Interfaces.Persistance;
using OGM.EmporosTest.Application.Common.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Application.Core.UnitsOfMeasure.Queries.ListUnitsOfMeasure
{
    public class ListUnitsOfMeasureQuery : IRequest<IEnumerable<UnitOfMeasureDto>>
    {
    }

    public class GetWeatherForecastsQueryHandler : IRequestHandler<ListUnitsOfMeasureQuery, IEnumerable<UnitOfMeasureDto>>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;
        public GetWeatherForecastsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        public async Task<IEnumerable<UnitOfMeasureDto>> Handle(ListUnitsOfMeasureQuery request, CancellationToken cancellationToken)
        {
            return await _context.UnitsOfMesure.ProjectTo<UnitOfMeasureDto>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken);
        }
    }
}
