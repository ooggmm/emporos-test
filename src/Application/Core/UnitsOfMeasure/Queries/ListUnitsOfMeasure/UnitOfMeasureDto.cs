﻿using OGM.EmporosTest.Application.Common.Mappings;
using OGM.EmporosTest.Domain.Entities;

namespace OGM.EmporosTest.Application.Core.UnitsOfMeasure.Queries.ListUnitsOfMeasure
{
    public class UnitOfMeasureDto : IMapFrom<UnitOfMesure>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int QuantityOfSingleUnits { get; set; }
    }
}
