﻿using OGM.EmporosTest.Domain.Common;
using System.Collections.Generic;

namespace OGM.EmporosTest.Domain.Entities
{
   public class Hospital : AuditableEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public IList<Pharmacy> Pharmacies { get; private set; } = new List<Pharmacy>();
    }
}
