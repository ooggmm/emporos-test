﻿using OGM.EmporosTest.Domain.Common;


namespace OGM.EmporosTest.Domain.Entities
{
    public class Item : AuditableEntity
    {
        /// <summary>
        ///   We can use the inherited Id as ItemNumber 
        /// </summary>
        //public int Number { get; set; } 
        public int ItemVendorId { get; set; }
        public string Upc { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public int MinimumOrderQuantity { get; set; }
        public int PurchaseUnitOfMesureId { get; set; }
        public UnitOfMesure PurchaseUnitOfMesure { get; set; }
        public ItemVendor ItemVendor { get; set; }
    }
}
