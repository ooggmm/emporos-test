﻿using OGM.EmporosTest.Domain.Common;
using System.Collections.Generic;

namespace OGM.EmporosTest.Domain.Entities
{
    public class ItemVendor : AuditableEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public IList<Item> Items { get; private set; } = new List<Item>();
    }
}
