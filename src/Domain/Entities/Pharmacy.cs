﻿using OGM.EmporosTest.Domain.Common;

namespace OGM.EmporosTest.Domain.Entities
{
    public class Pharmacy : AuditableEntity
    {
        public int HospitalId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public Hospital Hospital { get; set; }
    }
}
