﻿using OGM.EmporosTest.Domain.Common;


namespace OGM.EmporosTest.Domain.Entities
{
    public class PharmacyInventory : AuditableEntity
    {
        public int ItemId { get; set; }
        public int PharmacyId { get; set; }


        public int QuantityOnHand { get; set; }
        public decimal UnitPrice { get; set; }
        
        public int ReorderQuantity { get; set; }
        public int SellingUnitOfMesureId { get; set; }

        public UnitOfMesure SellingUnitOfMesure { get; set; }
        public Pharmacy Pharmacy { get; set; }

        public Item Item { get; set; }
    }
}
