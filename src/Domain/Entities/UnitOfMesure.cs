﻿using OGM.EmporosTest.Domain.Common;

namespace OGM.EmporosTest.Domain.Entities
{
    public class UnitOfMesure : AuditableEntity
    {
        public string Name { get; set; }
        public int QuantityOfSingleUnits { get; set; }
    }
}
