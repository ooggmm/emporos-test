﻿using Serilog.Core;
using Serilog.Events;

namespace OGM.EmporosTest.Infrastructure.Logs
{
    public static class LoggingLevelSwitchs
    {
        public static LoggingLevelSwitch BaseLevelSwitch { get; set; } = new LoggingLevelSwitch() { MinimumLevel = LogEventLevel.Information };
        public static LoggingLevelSwitch MicrosoftLevelSwitch { get; set; } = new LoggingLevelSwitch() { MinimumLevel = LogEventLevel.Information };
        public static LoggingLevelSwitch MicrosoftEntityFrameworkCoretLevelSwitch { get; set; } = new LoggingLevelSwitch() { MinimumLevel = LogEventLevel.Information };
        public static LoggingLevelSwitch MicrosoftHostingLifetimeLevelSwitch { get; set; } = new LoggingLevelSwitch() { MinimumLevel = LogEventLevel.Information };
    }
}
