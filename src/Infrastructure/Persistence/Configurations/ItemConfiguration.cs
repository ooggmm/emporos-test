﻿using OGM.EmporosTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OGM.EmporosTest.Infrastructure.Persistence.Configurations
{
    public class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.Property(t => t.ItemVendorId)
                    .IsRequired();
            builder.Property(t => t.Upc)
                .HasMaxLength(12)
                .IsRequired();
            builder.Property(t => t.Description)
                .HasMaxLength(100)
                .IsRequired();

            builder.Property(t => t.MinimumOrderQuantity)
                .IsRequired();
            builder.Property(t => t.Cost)
                .HasColumnType("decimal(10,2)")
                .IsRequired();

            builder.Property(t => t.PurchaseUnitOfMesureId)
                .IsRequired();

            builder.Property(t => t.Created)
                .IsRequired();
            builder.Property(t => t.CreatedBy)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(t => t.LastModifiedBy)
                .HasMaxLength(30);
        }
    }
}
