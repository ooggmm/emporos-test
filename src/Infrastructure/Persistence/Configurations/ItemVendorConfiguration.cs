﻿using OGM.EmporosTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OGM.EmporosTest.Infrastructure.Persistence.Configurations
{
    public class ItemVendorConfiguration : IEntityTypeConfiguration<ItemVendor>
    {
        public void Configure(EntityTypeBuilder<ItemVendor> builder)
        {
            builder.Property(t => t.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(t => t.Address)
                .HasMaxLength(200)
                .IsRequired();
            builder.Property(t => t.Created)
                .IsRequired();
            builder.Property(t => t.CreatedBy)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(t => t.LastModifiedBy)
                .HasMaxLength(30);
        }
    }
}
