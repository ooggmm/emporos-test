﻿using OGM.EmporosTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OGM.EmporosTest.Infrastructure.Persistence.Configurations
{
    public class PharmacyInventoryConfiguration : IEntityTypeConfiguration<PharmacyInventory>
    {
        public void Configure(EntityTypeBuilder<PharmacyInventory> builder)
        {

            builder.HasIndex(x => new { x.ItemId, x.PharmacyId }).IsUnique();
            builder.HasOne(x => x.SellingUnitOfMesure).WithMany().HasForeignKey(x => x.SellingUnitOfMesureId).OnDelete(DeleteBehavior.Restrict);

            builder.Property(t => t.ItemId)
                .IsRequired();
            builder.Property(t => t.PharmacyId)
                .IsRequired();

            builder.Property(t => t.QuantityOnHand)
                .IsRequired();
            builder.Property(t => t.UnitPrice)
                .HasColumnType("decimal(10,2)")
                .IsRequired();
            builder.Property(t => t.ReorderQuantity)
                .IsRequired();
            builder.Property(t => t.SellingUnitOfMesureId)
                .IsRequired();

            builder.Property(t => t.Created)
                .IsRequired();
            builder.Property(t => t.CreatedBy)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(t => t.LastModifiedBy)
                .HasMaxLength(30);
        }
    }
}
