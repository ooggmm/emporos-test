﻿using OGM.EmporosTest.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace OGM.EmporosTest.Infrastructure.Persistence.Configurations
{
    public class UnitOfMesureConfiguration : IEntityTypeConfiguration<UnitOfMesure>
    {
        public void Configure(EntityTypeBuilder<UnitOfMesure> builder)
        {
            builder.HasIndex(t => t.Name).IsUnique();

            builder.Property(t => t.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder.Property(t => t.QuantityOfSingleUnits)
                .IsRequired();

            builder.Property(t => t.Created)
              .IsRequired();
            builder.Property(t => t.CreatedBy)
                .HasMaxLength(30)
                .IsRequired();
            builder.Property(t => t.LastModifiedBy)
                .HasMaxLength(30);
        }
    }
    
}
