﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OGM.EmporosTest.Infrastructure.Persistence.Migrations.Application
{
    public partial class CreateInitialApplicationScheme : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Hospitals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Address = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Hospitals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ItemVendors",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Address = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemVendors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnitsOfMesure",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    QuantityOfSingleUnits = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnitsOfMesure", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Pharmacies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    HospitalId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Address = table.Column<string>(maxLength: 200, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pharmacies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Pharmacies_Hospitals_HospitalId",
                        column: x => x.HospitalId,
                        principalTable: "Hospitals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    ItemVendorId = table.Column<int>(nullable: false),
                    Upc = table.Column<string>(maxLength: 12, nullable: false),
                    Description = table.Column<string>(maxLength: 100, nullable: false),
                    Cost = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    MinimumOrderQuantity = table.Column<int>(nullable: false),
                    PurchaseUnitOfMesureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_ItemVendors_ItemVendorId",
                        column: x => x.ItemVendorId,
                        principalTable: "ItemVendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Items_UnitsOfMesure_PurchaseUnitOfMesureId",
                        column: x => x.PurchaseUnitOfMesureId,
                        principalTable: "UnitsOfMesure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PharmaciesInventory",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreatedBy = table.Column<string>(maxLength: 30, nullable: false),
                    Created = table.Column<DateTime>(nullable: false),
                    LastModifiedBy = table.Column<string>(maxLength: 30, nullable: true),
                    LastModified = table.Column<DateTime>(nullable: true),
                    ItemId = table.Column<int>(nullable: false),
                    PharmacyId = table.Column<int>(nullable: false),
                    QuantityOnHand = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    ReorderQuantity = table.Column<int>(nullable: false),
                    SellingUnitOfMesureId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PharmaciesInventory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PharmaciesInventory_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PharmaciesInventory_Pharmacies_PharmacyId",
                        column: x => x.PharmacyId,
                        principalTable: "Pharmacies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PharmaciesInventory_UnitsOfMesure_SellingUnitOfMesureId",
                        column: x => x.SellingUnitOfMesureId,
                        principalTable: "UnitsOfMesure",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Hospitals",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, "Address 1", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Hospital A" },
                    { 2, "Address 2", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Hospital B" },
                    { 3, "Address 3", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Hospital C" }
                });

            migrationBuilder.InsertData(
                table: "ItemVendors",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, "Address 1", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Vendor A" },
                    { 2, "Address 2", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Vendor B" },
                    { 3, "Address 3", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Vendor C" }
                });

            migrationBuilder.InsertData(
                table: "UnitsOfMesure",
                columns: new[] { "Id", "Created", "CreatedBy", "LastModified", "LastModifiedBy", "Name", "QuantityOfSingleUnits" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Unit", 1 },
                    { 2, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Dozen", 12 },
                    { 3, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Box20x", 20 },
                    { 4, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", null, null, "Box50x", 50 }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "Cost", "Created", "CreatedBy", "Description", "ItemVendorId", "LastModified", "LastModifiedBy", "MinimumOrderQuantity", "PurchaseUnitOfMesureId", "Upc" },
                values: new object[,]
                {
                    { 2, 10m, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", "Item B", 1, null, null, 100, 1, "432143214321" },
                    { 1, 5.5m, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", "Item A", 1, null, null, 20, 2, "123412341234" },
                    { 3, 3m, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", "Item C", 2, null, null, 500, 3, "111122223333" }
                });

            migrationBuilder.InsertData(
                table: "Pharmacies",
                columns: new[] { "Id", "Address", "Created", "CreatedBy", "HospitalId", "LastModified", "LastModifiedBy", "Name" },
                values: new object[,]
                {
                    { 1, "Address 1", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 1, null, null, "Pharmacy A" },
                    { 2, "Address 2", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 1, null, null, "Pharmacy B" },
                    { 3, "Address 3", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 2, null, null, "Pharmacy C" },
                    { 4, "Address 4", new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 3, null, null, "Pharmacy D" }
                });

            migrationBuilder.InsertData(
                table: "PharmaciesInventory",
                columns: new[] { "Id", "Created", "CreatedBy", "ItemId", "LastModified", "LastModifiedBy", "PharmacyId", "QuantityOnHand", "ReorderQuantity", "SellingUnitOfMesureId", "UnitPrice" },
                values: new object[,]
                {
                    { 3, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 2, null, null, 1, 100, 30, 1, 13m },
                    { 4, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 2, null, null, 3, 100, 40, 1, 11m },
                    { 1, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 1, null, null, 1, 100, 10, 1, 10m },
                    { 2, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 1, null, null, 2, 100, 20, 1, 13m },
                    { 5, new DateTime(2021, 1, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Seeder", 3, null, null, 1, 100, 50, 1, 12m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_ItemVendorId",
                table: "Items",
                column: "ItemVendorId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PurchaseUnitOfMesureId",
                table: "Items",
                column: "PurchaseUnitOfMesureId");

            migrationBuilder.CreateIndex(
                name: "IX_Pharmacies_HospitalId",
                table: "Pharmacies",
                column: "HospitalId");

            migrationBuilder.CreateIndex(
                name: "IX_PharmaciesInventory_PharmacyId",
                table: "PharmaciesInventory",
                column: "PharmacyId");

            migrationBuilder.CreateIndex(
                name: "IX_PharmaciesInventory_SellingUnitOfMesureId",
                table: "PharmaciesInventory",
                column: "SellingUnitOfMesureId");

            migrationBuilder.CreateIndex(
                name: "IX_PharmaciesInventory_ItemId_PharmacyId",
                table: "PharmaciesInventory",
                columns: new[] { "ItemId", "PharmacyId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UnitsOfMesure_Name",
                table: "UnitsOfMesure",
                column: "Name",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PharmaciesInventory");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Pharmacies");

            migrationBuilder.DropTable(
                name: "ItemVendors");

            migrationBuilder.DropTable(
                name: "UnitsOfMesure");

            migrationBuilder.DropTable(
                name: "Hospitals");
        }
    }
}
