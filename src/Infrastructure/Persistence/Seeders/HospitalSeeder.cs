﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{
    public class HospitalSeeder : IEntityTypeConfiguration<Hospital>
    {
        public void Configure(EntityTypeBuilder<Hospital> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new Hospital { Created = date, CreatedBy = "Seeder", Id = 1, Name = "Hospital A", Address = "Address 1" },
                new Hospital { Created = date, CreatedBy = "Seeder", Id = 2, Name = "Hospital B", Address = "Address 2" },
                new Hospital { Created = date, CreatedBy = "Seeder", Id = 3, Name = "Hospital C", Address = "Address 3" }
                );
        }
    }
}
