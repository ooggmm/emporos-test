﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{
    public class ItemSeeder : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new Item { Created = date, CreatedBy = "Seeder", Id = 1, Description = "Item A", Cost = 5.5M , MinimumOrderQuantity  = 20, PurchaseUnitOfMesureId = 2 , Upc = "123412341234", ItemVendorId = 1},
                new Item { Created = date, CreatedBy = "Seeder", Id = 2, Description = "Item B", Cost = 10M, MinimumOrderQuantity = 100, PurchaseUnitOfMesureId = 1, Upc = "432143214321", ItemVendorId = 1 },
                new Item { Created = date, CreatedBy = "Seeder", Id = 3, Description = "Item C", Cost = 3M, MinimumOrderQuantity = 500, PurchaseUnitOfMesureId = 3, Upc = "111122223333", ItemVendorId = 2 }
                );
        }
    }
}
