﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{
    public class ItemVendorSeeder : IEntityTypeConfiguration<ItemVendor>
    {
        public void Configure(EntityTypeBuilder<ItemVendor> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new ItemVendor { Created = date, CreatedBy = "Seeder", Id = 1, Name = "Vendor A", Address = "Address 1" },
                new ItemVendor { Created = date, CreatedBy = "Seeder", Id = 2, Name = "Vendor B", Address = "Address 2" },
                new ItemVendor { Created = date, CreatedBy = "Seeder", Id = 3, Name = "Vendor C", Address = "Address 3" }
                );
        }
    }
}
