﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{

    public class PharmacyInventorySeeder : IEntityTypeConfiguration<PharmacyInventory>
    {
        public void Configure(EntityTypeBuilder<PharmacyInventory> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new PharmacyInventory { Created = date, CreatedBy = "Seeder", Id = 1, ItemId = 1, PharmacyId = 1, QuantityOnHand = 100, ReorderQuantity = 10, SellingUnitOfMesureId = 1, UnitPrice = 10M },
                new PharmacyInventory { Created = date, CreatedBy = "Seeder", Id = 2, ItemId = 1, PharmacyId = 2, QuantityOnHand = 100, ReorderQuantity = 20, SellingUnitOfMesureId = 1, UnitPrice = 13M },
                new PharmacyInventory { Created = date, CreatedBy = "Seeder", Id = 3, ItemId = 2, PharmacyId = 1, QuantityOnHand = 100, ReorderQuantity = 30, SellingUnitOfMesureId = 1, UnitPrice = 13M },
                new PharmacyInventory { Created = date, CreatedBy = "Seeder", Id = 4, ItemId = 2, PharmacyId = 3, QuantityOnHand = 100, ReorderQuantity = 40, SellingUnitOfMesureId = 1, UnitPrice = 11M },
                new PharmacyInventory { Created = date, CreatedBy = "Seeder", Id = 5, ItemId = 3, PharmacyId = 1, QuantityOnHand = 100, ReorderQuantity = 50, SellingUnitOfMesureId = 1, UnitPrice = 12M }
                );
        }
    }
}
