﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{

    public class PharmacySeeder : IEntityTypeConfiguration<Pharmacy>
    {
        public void Configure(EntityTypeBuilder<Pharmacy> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new Pharmacy { Created = date, CreatedBy = "Seeder", Id = 1, Name = "Pharmacy A" , HospitalId = 1, Address = "Address 1" },
                new Pharmacy { Created = date, CreatedBy = "Seeder", Id = 2, Name = "Pharmacy B" , HospitalId = 1, Address = "Address 2" },
                new Pharmacy { Created = date, CreatedBy = "Seeder", Id = 3, Name = "Pharmacy C" , HospitalId = 2, Address = "Address 3" },
                new Pharmacy { Created = date, CreatedBy = "Seeder", Id = 4, Name = "Pharmacy D" , HospitalId = 3, Address = "Address 4" }
                );
        }
    }
}
