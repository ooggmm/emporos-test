﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OGM.EmporosTest.Domain.Entities;
using System;

namespace OGM.EmporosTest.Infrastructure.Persistence.Seeders
{

    public class UnitOfMesureSeeder : IEntityTypeConfiguration<UnitOfMesure>
    {
        public void Configure(EntityTypeBuilder<UnitOfMesure> builder)
        {
            var date = new DateTime(2021, 1, 5);
            builder.HasData(
                new UnitOfMesure { Created = date, CreatedBy = "Seeder", Id = 1, Name = "Unit", QuantityOfSingleUnits = 1 },
                new UnitOfMesure { Created = date, CreatedBy = "Seeder", Id = 2, Name = "Dozen", QuantityOfSingleUnits = 12 },
                new UnitOfMesure { Created = date, CreatedBy = "Seeder", Id = 3, Name = "Box20x", QuantityOfSingleUnits = 20 },
                new UnitOfMesure { Created = date, CreatedBy = "Seeder", Id = 4, Name = "Box50x", QuantityOfSingleUnits = 50 }
                ); 
        }
    }
}
