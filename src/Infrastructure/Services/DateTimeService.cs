﻿using OGM.EmporosTest.Application.Common.Interfaces;
using System;

namespace OGM.EmporosTest.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}
