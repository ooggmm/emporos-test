﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OGM.EmporosTest.Application.Common.Exceptions;
using OGM.EmporosTest.WebApi.Controllers.Common;
using System;
using OGM.EmporosTest.Application.Core.Items.Commands.CreateItem;
using OGM.EmporosTest.Application.Core.Items.Commands.UpdateItem;
using Microsoft.AspNetCore.Authorization;

namespace OGM.EmporosTest.WebApi.Controllers.v1_0
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [ApiVersion("1.0")]
    public class ItemsController : ApiController
    {
        public ItemsController()
        {
        }

        [HttpPost("Create")]
        public async Task<ActionResult> Post(CreateItemCommand command)
        {
            try
            {
                var result = await Mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Validation Error", ex.ValidationResponse));
            }
            catch (Exception ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Unhandled Exception", ex.Message));
            }
        }
        
        [HttpPut("Update")]
        public async Task<ActionResult> Post(UpdateItemCommand command)
        {
            try
            {
                var result = await Mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Validation Error", ex.ValidationResponse));
            }
            catch (Exception ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Unhandled Exception", ex.Message));
            }
        }
        

    }
}
