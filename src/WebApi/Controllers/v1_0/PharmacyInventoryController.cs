﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.CreatePharmacyInventoryRecord;
using OGM.EmporosTest.Application.Common.Exceptions;
using OGM.EmporosTest.WebApi.Controllers.Common;
using System;
using OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.UpdatePharmacyInventoryRecord;
using OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.DeletePharmacyInventoryRecord;
using Microsoft.AspNetCore.Authorization;

namespace OGM.EmporosTest.WebApi.Controllers.v1_0
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [ApiVersion("1.0")]
    public class PharmacyInventoryController : ApiController
    {
        public PharmacyInventoryController()
        {
        }

        [HttpPost("Create")]
        public async Task<ActionResult> Post(CreatePharmacyInventoryRecordCommand command)
        {
            try
            {
                var result = await Mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Validation Error", ex.ValidationResponse));
            }
            catch (Exception ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Unhandled Exception", ex.Message));
            }
        }

        [HttpPut("Update")]
        public async Task<ActionResult> Post(UpdatePharmacyInventoryRecordCommand command)
        {
            try
            {
                var result = await Mediator.Send(command);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Validation Error", ex.ValidationResponse));
            }
            catch (Exception ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Unhandled Exception", ex.Message));
            }
        }

        [HttpDelete("Delete/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var result = await Mediator.Send(new DeletePharmacyInventoryRecordCommand {Id = id});
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Validation Error", ex.ValidationResponse));
            }
            catch (Exception ex)
            {
                return BadRequest(Application.Common.Models.Response.Fail("Unhandled Exception", ex.Message));
            }
        }
    }
}
