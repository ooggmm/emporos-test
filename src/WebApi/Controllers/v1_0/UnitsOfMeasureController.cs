﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OGM.EmporosTest.Application.Core.UnitsOfMeasure.Queries.ListUnitsOfMeasure;
using OGM.EmporosTest.WebApi.Controllers.Common;

namespace OGM.EmporosTest.WebApi.Controllers.v1_0
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [ApiVersion("1.0")]
    public class UnitsOfMeasureController : ApiController
    {

        [HttpGet]        
        public async Task<ActionResult> Get()
        {
            var result = await Mediator.Send(new ListUnitsOfMeasureQuery());
            return Ok(result);
        }

    }
}
