﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OGM.EmporosTest.Application.Core.UnitsOfMeasure.Queries.ListUnitsOfMeasure;
using OGM.EmporosTest.WebApi.Controllers.Common;

namespace OGM.EmporosTest.WebApi.Controllers.v2_0
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]
    [ApiVersion("2.0")]
    public class UnitsOfMeasureController : ApiController
    {

        [HttpGet]        
        public string GetV2()
        {
            return "version2";
        }
    }
}
