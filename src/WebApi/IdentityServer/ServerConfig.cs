﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace OGM.EmporosTest.WebApi.IdentityServer
{
    public class ServerConfig
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email()
            };
        }


        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("apiEmporos", "API Emporos")
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("apiEmporos", "API Emporos")
                {
                     Scopes = new []{ "apiEmporos" }
                }
            };
        }
        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {

                // resource owner password grant client
                new Client
                {
                    ClientId = "oscar_client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("oscar_secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "apiEmporos" }
                }
            };
        }
    }
}
