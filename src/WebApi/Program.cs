using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OGM.EmporosTest.Infrastructure.Logs;
using Serilog;
using System;
using System.Threading.Tasks;

namespace OGM.EmporosTest.WebApi
{
    public class Program
    {
        


        public async static Task Main(string[] args)
        {
            try
            {
                var host = CreateHostBuilder(args).Build();
                AddLogger(host);

                Log.Information("Starting up");
                await host.RunAsync();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Application start-up failed");
            }
            finally
            {
                Log.CloseAndFlush();
            }

        }
        private static void AddLogger(IHost host)
        {
            
            using var scope = host.Services.CreateScope();
            var services = scope.ServiceProvider;
            var configuration = services.GetRequiredService<IConfiguration>();
            var logPath = configuration.GetValue<string>("Logging:FileName");
            var template = "[{Timestamp:yyyy-MM-dd HH:mm:ss.fff}] [{Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}";
            Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .MinimumLevel.ControlledBy(LoggingLevelSwitchs.BaseLevelSwitch)
            .MinimumLevel.Override("Microsoft", LoggingLevelSwitchs.MicrosoftLevelSwitch)
            .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LoggingLevelSwitchs.MicrosoftEntityFrameworkCoretLevelSwitch)
            .MinimumLevel.Override("Microsoft.Hosting.Lifetime", LoggingLevelSwitchs.MicrosoftHostingLifetimeLevelSwitch)
            .WriteTo.Async(x => x.Console(outputTemplate: template))
            .WriteTo.Async(x => x.File(path: logPath,
                                shared: false,
                                rollingInterval: RollingInterval.Day,
                                levelSwitch: LoggingLevelSwitchs.BaseLevelSwitch,
                                outputTemplate: template))
            .CreateLogger();
        }


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
