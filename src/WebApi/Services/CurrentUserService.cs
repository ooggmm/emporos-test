﻿using Microsoft.AspNetCore.Http;
using OGM.EmporosTest.Application.Common.Interfaces;
using System.Security.Claims;

namespace OGM.EmporosTest.WebApi.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string UserId => _httpContextAccessor.HttpContext?.User?.FindFirstValue("client_id");
    }

}
