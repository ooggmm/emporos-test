using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OGM.EmporosTest.Application;
using OGM.EmporosTest.Application.Common.Interfaces;
using OGM.EmporosTest.Infrastructure;
using OGM.EmporosTest.WebApi.IdentityServer;
using OGM.EmporosTest.WebApi.Services;
using System;
using System.Linq;

namespace OGM.EmporosTest.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddApplication();
            services.AddInfrastructure(Configuration);
            services.AddScoped<ICurrentUserService, CurrentUserService>();




            services.AddHttpContextAccessor();
            services.AddControllers();

            services.AddHealthChecks();

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });


            services.AddSwaggerGen(c =>
            {
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Emporos API",
                    Description = "A simple example ASP.NET Core Web API",
                    TermsOfService = new Uri("https://www.rayatron.tech/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "S",
                        Email = string.Empty
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("https://www.rayatron.tech/license"),
                    }
                });
            });


            var builder = services.AddIdentityServer(options =>
            {
                options.EmitStaticAudienceClaim = true;
            })
           .AddInMemoryIdentityResources(ServerConfig.GetIdentityResources())
           .AddInMemoryApiScopes(ServerConfig.GetApiScopes())
           .AddInMemoryApiResources(ServerConfig.GetApiResources())
           .AddInMemoryClients(ServerConfig.GetClients());
            builder.AddDeveloperSigningCredential();

            services.AddAuthentication("Bearer")
            .AddJwtBearer("Bearer", options =>
            {
                options.Authority = "https://localhost:5001";
                //options.RequireHttpsMetadata = false;
                options.Audience = "apiEmporos";

            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseHealthChecks("/health");
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Emporos API");
            });
            
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseIdentityServer();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
