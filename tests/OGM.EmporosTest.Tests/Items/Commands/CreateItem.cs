using FluentAssertions;
using NUnit.Framework;
using OGM.EmporosTest.Application.Core.Items.Commands.CreateItem;
using OGM.EmporosTest.Application.Common.Exceptions;

namespace OGM.EmporosTest.Tests.Commands
{
    using static Testing;
    public class CreateItem : TestBase
    {
        [Test]
        public void UpcLengthValidation()
        {
            var command = new CreateItemCommand
            {
                Cost = 100,
                Description = "Item Test",
                ItemVendorId = 1,
                PurchaseUnitOfMesureId = 1,
                MinimumOrderQuantity = 1000,
                Upc = "123456"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

    }
}