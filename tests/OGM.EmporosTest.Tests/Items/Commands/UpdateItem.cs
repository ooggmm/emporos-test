using FluentAssertions;
using NUnit.Framework;
using OGM.EmporosTest.Application.Common.Exceptions;
using OGM.EmporosTest.Application.Core.Items.Commands.UpdateItem;

namespace OGM.EmporosTest.Tests.Commands
{
    using static Testing;
    public class UpdateItem : TestBase
    {
        [Test]
        public void UpcLengthValidation()
        {
            var command = new UpdateItemCommand
            {
                Id = 1,
                Cost = 100,
                Description = "updated Item",
                ItemVendorId = 1,
                PurchaseUnitOfMesureId = 1,
                MinimumOrderQuantity = 1000,
                Upc = "123456"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

    
    }
}