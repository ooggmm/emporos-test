using FluentAssertions;
using NUnit.Framework;
using OGM.EmporosTest.Application.Common.Exceptions;
using OGM.EmporosTest.Application.Core.PharmaciesInventory.Commands.CreatePharmacyInventoryRecord;

namespace OGM.EmporosTest.Tests
{
    using static Testing;
    public class CreatePharmacyInventory : TestBase
    {
        [Test]
        public void ExistsItemValidation()
        {
            var command = new CreatePharmacyInventoryRecordCommand
            {
                ItemId = 99999,
                PharmacyId = 1,
                QuantityOnHand = 10,
                ReorderQuantity = 1000,
                SellingUnitOfMesureId = 1,
                UnitPrice = 60M
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public void ExistsPharmacyValidation()
        {
            var command = new CreatePharmacyInventoryRecordCommand
            {
                ItemId = 1,
                PharmacyId = 9999,
                QuantityOnHand = 10,
                ReorderQuantity = 1000,
                SellingUnitOfMesureId = 1,
                UnitPrice = 60M
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

    }
}