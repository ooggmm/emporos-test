﻿using NUnit.Framework;
using System.Threading.Tasks;

namespace OGM.EmporosTest.Tests
{
    using static Testing;
    public class TestBase
    {
        [SetUp]
        public async Task TestSetUp()
        {
            await ResetState();
        }
    }
}
